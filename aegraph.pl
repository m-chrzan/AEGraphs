:- use_module(library(lists)).

jestWyborem([], []).
jestWyborem([[V, T] | Rest], [[V, T] | Rest2]) :-
    jestWyborem(Rest, Rest2).
jestWyborem([[V, a | Neighbors] | Rest], [[V, a | Neighbors] | Rest2]) :-
    jestWyborem(Rest, Rest2).
jestWyborem([[V, e | Neighbors] | Rest], [[V, e, W] | Rest2]) :-
    member(W, Neighbors),
    jestWyborem(Rest, Rest2).

% neighbors(Graph, Vertex, Neighbors) says that Neighbors is the list of
% Vertex's neighbors in Graph
neighbors(Graph, V, Neighbors) :-
    select([V, _ | Neighbors], Graph, _).

% isDfs(G, L, S, V) says that
% - L is the suffix of a DFS over G
% - S is the stack of vertices that need to be visited next
% - V is the list of vertices that have already been visited
isDfs(_, [], [], _).
isDfs(Graph, Dfs, [Top | Stack], Visited) :-
    memberchk(Top, Visited),
    isDfs(Graph, Dfs, Stack, Visited).
isDfs(Graph, [Current | Tail], [Current | Stack], Visited) :-
    \+ memberchk(Current, Visited),
    neighbors(Graph, Current, Neighbors),
    permutation(Neighbors, NeighborsPermutation),
    append(NeighborsPermutation, Stack, NewStack),
    isDfs(Graph, Tail, NewStack, [Current | Visited]).

jestDFS([], []).
jestDFS([[V, T | Neighbors] | Rest], Dfs) :-
    isDfs([[V, T | Neighbors] | Rest], Dfs, [V], []).

jestADFS(AEGraph, Dfs) :-
    jestWyborem(AEGraph, Choice),
    jestDFS(Choice, Dfs).

% aeNeighbors(Graph, Vertex, Neighbors) says that
% - if Vertex is an A-vertex, Neighbors is the list of all of Vertex's neighbors
% - if Vertex is an E-vertex, Neighbors is a singleton list containing one of
%   Vertex's neighbors, or empty if it has no neighbors
aeNeighbors(Graph, V, Neighbors) :-
    select([V, a | Neighbors], Graph, _).
aeNeighbors(Graph, V, [Neighbor]) :-
    select([V, e | Neighbors], Graph, _),
    select(Neighbor, Neighbors, _).
aeNeighbors(Graph, V, []) :-
    select([V, e], Graph, _).

% isDfs(G, L, S, V) says that
% - L is the suffix of a DFS over a choice in G
% - S is the stack of vertices that need to be visited next
% - V is the list of vertices that have already been visited
isAeDfs(_, [], [], _).
isAeDfs(Graph, Dfs, [Top | Stack], Visited) :-
    memberchk(Top, Visited),
    isAeDfs(Graph, Dfs, Stack, Visited).
isAeDfs(Graph, [Current | Tail], [Current | Stack], Visited) :-
    \+ memberchk(Current, Visited),
    aeNeighbors(Graph, Current, Neighbors),
    permutation(Neighbors, NeighborsPermutation),
    append(NeighborsPermutation, Stack, NewStack),
    isAeDfs(Graph, Tail, NewStack, [Current | Visited]).

jestADFS1([], []).
jestADFS1([[V, T | Neighbors] | Rest], Dfs) :-
    isAeDfs([[V, T | Neighbors] | Rest], Dfs, [V], []).
